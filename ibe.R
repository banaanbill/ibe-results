source('measures.R')
source('myplots.R')

data <- list(
	rename(read.table("latest-emea.tsv", header=TRUE)),
	rename(read.table("latest-firewall1.tsv", header=TRUE)),
	rename(read.table("latest-firewall2.tsv", header=TRUE)),
	rename(read.table("latest-healthcare.tsv", header=TRUE)),
	rename(read.table("latest-university_large.tsv", header=TRUE)),
	rename(read.table("latest-domino.tsv", header=TRUE))
)
names(data) <- c('emea', 'firewall1', 'firewall2', 'healthcare', 'university', 'domino')
namecolors(data)

pdf(width=5.5, height=5.5)
par(las=3)

myplot(data, 'PtIbe.AddBias', 'PtIbe.FileRekeyRevU')

#myboxplot(data, 'PtIbe.FileRekeyPerUserRevoked')

oldparmar <- par('mar')
par(mar=c(5, 4, 4, 3) + 0.1)
myboxplot(data[c('emea', 'firewall1', 'firewall2')], 'PtIbe.FileRekeyPerUserRevoked', count=7, cex.axis=0.8)
abline(v=4)
par(new=T)
myboxplot(data[c('healthcare', 'university', 'domino')], 'PtIbe.FileRekeyPerUserRevoked', count=7, start=5, yaxt='n', main='', ylab='', xlab='', cex.axis=0.8, frame.plot=F)
axis(4, cex.axis=0.8)
par(mar=oldparmar)

myplot(data, 'PtIbe.AddBias', 'PtIbe.IbeEncRevU')

#myboxplot(data, 'PtIbe.IbeEncPerUserRevoked')

oldparmar <- par('mar')
par(mar=c(5, 4, 4, 3) + 0.1)
myboxplot(data[c('emea', 'firewall1', 'firewall2')], 'PtIbe.IbeEncPerUserRevoked', count=7, cex.axis=0.8)
abline(v=4)
par(new=T)
myboxplot(data[c('healthcare', 'university', 'domino')], 'PtIbe.IbeEncPerUserRevoked', count=7, start=5, yaxt='n', main='', ylab='', xlab='', cex.axis=0.8, frame.plot=F)
axis(4, cex.axis=0.8)
par(mar=oldparmar)

myboxplot(data, 'PtIbe.IbeEncPerPermRevoked', cex.axis=0.8)

onesetplot(data[c('firewall1')], 'PtIbe.AddBias', c('PtIbe.IbeEncSu', 'PubKey.AsymEncSu'), ygroup='Key encryptions')


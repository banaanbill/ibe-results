.PHONY: all clean

figs = \
rekeys-user-scatter.pdf \
rekeys-user-box.pdf \
ibe-user-scatter.pdf \
ibe-user-box.pdf \
ibe-perm-box.pdf \
encryptions-scatter.pdf

data = \
latest-domino.tsv \
latest-emea.tsv \
latest-firewall1.tsv \
latest-firewall2.tsv \
latest-healthcare.tsv \
latest-university_large.tsv

all: $(figs)

Rplots.pdf: ibe.R myplots.R measures.R $(data)
	R CMD BATCH ibe.R

rekeys-user-scatter.pdf: Rplots.pdf
	pdftk "$<" cat 1 output "$@"
rekeys-user-box.pdf: Rplots.pdf
	pdftk "$<" cat 2 output "$@"
ibe-user-scatter.pdf: Rplots.pdf
	pdftk "$<" cat 3 output "$@"
ibe-user-box.pdf: Rplots.pdf
	pdftk "$<" cat 4 output "$@"
ibe-perm-box.pdf: Rplots.pdf
	pdftk "$<" cat 5 output "$@"
encryptions-scatter.pdf: Rplots.pdf
	pdftk "$<" cat 6 output "$@"

clean:
	rm -f *.pdf *.Rout .RData
